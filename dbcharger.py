#! /usr/bin/python
# dbcharger.py handles the charge and update of a database
# Copyright (C) 2020 Pizzabanana

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import getpass
import glob
import logging
import re
import sqlalchemy
import sqlalchemy_utils
import sys

PROGRAM_NAME = "dbcharger"
PROGRAM_DESCRIPTION = "Handles the charge and update of the database"
MAIN_DATABASE = "postgres"
DATABASE_ENGINE = "postgresql"
DEFAULT_PORT = "5432"
BASELINE_FILE = "baseline.sql"
UPDATES_DIR = "updates"
UPDATES_PATTERN = "patch-[!-]*-[!-]*-[!-]*.sql"
UPDATES_REGEX = "patch-([^-]+)-([^-]+)-([^-]+).sql"
FUNCTIONS_DIR = "functions"
FUNCTIONS_PATTERN = "*.sql"
VIEWS_DIR = "views"
VIEWS_PATTERNS = "*.sql"
TRIGGERS_DIR = "triggers"
TRIGGERS_PATTERN = "*.sql"
LOGGING_LEVEL = logging.INFO
LOGGING_FORMAT = '%(asctime)s {} %(levelname)s: %(message)s'.format(
    PROGRAM_NAME)


def argparser():
    """Builds the argument parser"""
    parser = argparse.ArgumentParser(
        prog=PROGRAM_NAME,
        description=PROGRAM_DESCRIPTION
    )

    parser.add_argument(
        '-d',
        '--database',
        type=str,
        help='database name',
        metavar='NAME',
        required=True
    )

    parser.add_argument(
        '-H',
        '--host',
        type=str,
        help='database host',
        metavar='DIR',
        required=True
    )

    parser.add_argument(
        '-s',
        '--schema',
        type=str,
        help='database schema',
        metavar='SCHEMA',
        required=True
    )

    parser.add_argument(
        '-i',
        '--ignore',
        action='store_true',
        help='ignores check of database existence'
    )

    parser.add_argument(
        '-p',
        '--password',
        type=str,
        help='database password (prompted if not passed)',
        metavar='PASS'
    )

    parser.add_argument(
        '-P',
        '--port',
        type=str,
        help='database port',
        metavar='PORT',
        default=DEFAULT_PORT
    )

    parser.add_argument(
        '-u',
        '--user',
        type=str,
        help='database user (session user if not passed)',
        metavar='USER',
        default=getpass.getuser()
    )

    return parser


def db_create(engine, db_name):
    """Creates a database"""
    conn = engine.connect()
    conn.execute("COMMIT")
    conn.execute("CREATE DATABASE " + db_name)
    conn.close()


def schema_exists(engine, schema):
    """Checks if a schema exists"""
    conn = engine.connect()
    return engine.dialect.has_schema(conn, schema)


def fetch_last_version(engine, schema):
    """Retrieves the database version"""

    rows = engine.execute(sqlalchemy.sql.text("""
        SELECT major_version, minor_version, build_version
            FROM %s.schema_version
            ORDER BY major_version DESC, minor_version DESC,
                build_version DESC
            LIMIT 1
    """ % schema))

    for row in rows:
        return row[0], row[1], row[2]


def run_script(engine, schema, script):
    """Runs a script"""
    with open(script, 'r') as f:
        contents = f.read().replace('{{schema}}', schema)

    engine.execute(sqlalchemy.sql.text(contents).execution_options(
        autocommit=True)
    )


if __name__ == "__main__":
    logging.basicConfig(
        format=LOGGING_FORMAT,
        level=LOGGING_LEVEL
    )

    # Parse arguments
    args = argparser().parse_args()

    if not args.password:
        try:
            args.password = getpass.getpass('Password for user %s: ' % (
                args.user)
            )
        except Exception:
            sys.exit(0)

    main_url = '%s://%s:%s@%s/%s' % (
        DATABASE_ENGINE,
        args.user,
        args.password,
        args.host,
        MAIN_DATABASE
    )

    sql_url = '%s://%s:%s@%s/%s' % (
        DATABASE_ENGINE,
        args.user,
        args.password,
        args.host,
        args.database
    )

    if args.ignore:
        db_exists = True
    else:
        try:
            db_exists = sqlalchemy_utils.database_exists(sql_url)
        except Exception as e:
            logging.critical('cannot connect to postgres server: %s' % str(e))
            sys.exit(1)

    # Connect to database, create if not exists
    create_baseline = False
    if db_exists:
        engine = sqlalchemy.create_engine(sql_url)
        try:
            create_baseline = not schema_exists(engine, args.schema)
            if create_baseline:
                logging.info('schema %s does not exists running baseline' % (
                    args.schema)
                )
        except Exception as e:
            logging.critical('cannot check if schema \'%s\' exists' % (
                args.schema)
            )
            sys.exit(1)
    else:
        create_baseline = True
        logging.info('database %s does not exists, creating' % args.database)
        logging.debug('connecting to database \'%s\'' % MAIN_DATABASE)
        engine = sqlalchemy.create_engine(main_url)
        try:
            db_create(engine, args.database)
        except Exception as e:
            logging.critical('error creating database \'%s\': %s' % (
                args.database,
                str(e))
            )
            sys.exit(2)

    logging.debug('connecting to database \'%s\'' % args.database)
    engine = sqlalchemy.create_engine(sql_url)

    # Load baseline
    if create_baseline:
        logging.info('loading baseline')
        try:
            run_script(engine, args.schema, BASELINE_FILE)
        except Exception as e:
            logging.critical('error loading baseline file \'%s\': %s' % (
                BASELINE_FILE,
                str(e))
            )
            sys.exit(3)

    # Retrieve version
    try:
        major_version, minor_version, build_version = fetch_last_version(
            engine,
            args.schema
        )
    except Exception as e:
        logging.error('cannot retrieve database version: %s' % str(e))
        sys.exit(4)
    logging.info('database version %d.%d.%d' % (
        major_version,
        minor_version,
        build_version)
    )

    # Run updates scripts
    restr = '%s.%s' % (UPDATES_DIR, UPDATES_REGEX)
    for update in sorted(glob.glob('%s/%s' % (UPDATES_DIR, UPDATES_PATTERN))):
        versions = re.match(restr, update)
        if versions:
            try:
                major = int(versions.group(1))
                minor = int(versions.group(2))
                build = int(versions.group(3))
            except Exception as e:
                logging.error('ignoring file \'%s\' bad format: %s' % (
                    update,
                    str(e))
                )
                continue
            if major > major_version or (
               major == major_version and minor > minor_version) or (
               major == major_version and minor == minor_version and
               build > build_version):
                logging.info('applying version %d.%d.%d script file \'%s\'' % (
                    major,
                    minor,
                    build,
                    update)
                )
                try:
                    run_script(engine, args.schema, update)
                except Exception as e:
                    logging.critical('error running script \'%s\': %s' % (
                        update,
                        str(e))
                    )
                    sys.exit(3)
        else:
            logging.error('ignoring file \'%s\'' % update)

    # Regenerate objects
    logging.info('regenerating functions')
    for function in sorted(glob.glob('%s/%s' % (
        FUNCTIONS_DIR,
        FUNCTIONS_PATTERN))
    ):
        try:
            run_script(engine, args.schema, function)
        except Exception as e:
            logging.critical('error running script \'%s\': %s' % (
                function,
                str(e))
            )
            sys.exit(3)

    logging.info('regenerating views')
    for view in sorted(glob.glob('%s/%s' % (
        VIEWS_DIR,
        VIEWS_PATTERNS))
    ):
        try:
            run_script(engine, args.schema, view)
        except Exception as e:
            logging.critical('error running script \'%s\': %s' % (
                view,
                str(e))
            )
            sys.exit(3)

    logging.info('regenerating triggers')
    for trigger in sorted(glob.glob('%s/%s' % (
        TRIGGERS_DIR,
        TRIGGERS_PATTERN))
    ):
        try:
            run_script(engine, args.schema, trigger)
        except Exception as e:
            logging.critical('error running script \'%s\': %s' % (
                trigger,
                str(e))
            )
            sys.exit(3)

    logging.info('database updated to the last version')

    sys.exit(0)
