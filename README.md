# DBCharger
> Script to charge a database

DBCharger implements a python script to charge a database. This repository
should be a git-submodule to database repositories.

* [Charge database](#charge-database)
* [Update database schema](#update-database-schema)
* [Update database functions](#update-database-functions)
* [Update database views](#update-database-views)
* [Update database triggers](#update-database-triggers)

## Charge database

To charge the database you should load the [baseline](baseline.sql) script, then
run the update patches in order. To do this you can use the
[dbcharger.py](dbcharger.py) script that will connect to the database, create or
update to the last version on the repository.

The usage is the following:

```bash
[user@host] $ python dbcharger.py -h
usage: dbcharger [-h] -d NAME -H DIR -s SCHEMA [-p PASS] [-P PORT] [-u USER]

Handles the charge and update of the database

optional arguments:
  -h, --help            show this help message and exit
  -d NAME, --database NAME
                        database name
  -H DIR, --host DIR    database host
  -s SCHEMA, --schema SCHEMA
                        database schema
  -p PASS, --password PASS
                        database password (prompted if not passed)
  -P PORT, --port PORT  database port
  -u USER, --user USER  database user (session user if not passed)
```

To create/update a local database set the following arguments:

```
[user@host] $ python charge.py -d database_name -H localhost -s schema_name
```

To create/update a remote database (on machine 192.168.0.54) set the following
arguments:

```
[user@host] $ python charge.py -d database_name -H 192.168.0.54 -s schema_name
```

## Update database schema

To update database schema, create a new patch file, with the version name on the
updates directory: *patch-1-0.1.sql*, *patch-1-1-0.sql*, *patch-2-1-4.sql*.

Be sure to include at the end the insert of the database version:

```sql
INSERT INTO {{schema}}.schema_version VALUES(
    DEFAULT,
    major,
    minor,
    build,
    'Description of the patch',
    DEFAULT
);
```

**NEVER** edit one patch file that it's already merged on master branch. This
would cause database inconsistences. Create a new patch instead.

## Update database functions

To update functions or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your function will be reloaded.

## Update database views

To update views or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your view will be reloaded.

## Update database triggers

To update triggers or create new ones, just edit the corresponding files or
create a new one. Be sure to include the DROP command at the start of the script
so your trigger will be reloaded.
